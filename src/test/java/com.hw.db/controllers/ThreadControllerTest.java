package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

class ThreadControllerTest {
    private ThreadController threadController;
    private Thread thread;
    private Post post;
    private User user;
    private final int id = 1234567;
    private final String slug = "slug";
    private final String username = "username";

    @BeforeEach
    public void init() {
        user = new User(username, "email@test.com", "Lastname Firstname", "some some some");
        post = new Post(username, Timestamp.from(Instant.now()), "forum", "message", 0, 0, false);
        thread = new Thread(username, Timestamp.from(Instant.now()), "forum", "message", slug, "title", 0);
        thread.setId(id);
        threadController = new ThreadController();
    }

    @Test
    @DisplayName("when given id or slug then return thread")
    public void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(id))
                    .thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(thread);

            Assertions.assertEquals(thread, threadController.CheckIdOrSlug(Integer.toString(id)));
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug(slug));
        }
    }

    @Test
    @DisplayName("when given id or slug then return created post")
    public void testCreatePost() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                threadDAOMock.when(() -> ThreadDAO.getThreadById(id))
                        .thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                        .thenReturn(thread);
                userDAOMock.when(() -> UserDAO.Info(username))
                        .thenReturn(user);

                ResponseEntity responseEntity = threadController.createPost(Integer.toString(id), Collections.singletonList(post));

                Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
                Assertions.assertEquals(Collections.singletonList(post), responseEntity.getBody());
            }
        }
    }

    @Test
    @DisplayName("when given id or slug then return filtered posts")
    public void testPosts() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> posts = Collections.singletonList(post);
            threadDAOMock.when(() -> ThreadDAO.getThreadById(id))
                    .thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getPosts(id, 1, 2020, "flat", true))
                    .thenReturn(posts);

            ResponseEntity responseEntity = threadController.Posts(slug, 1, 2020, "flat", true);

            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(posts, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("when given id or slug then return changed thread")
    public void testChange() {
        Thread threadToChange =
                new Thread(username, new Timestamp(0), "forum", "message", "slug1", "title", 0);
        threadToChange.setId(123);
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(id))
                    .thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadById(123))
                    .thenReturn(threadToChange);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug1"))
                    .thenReturn(threadToChange);

            ResponseEntity responseEntity = threadController.change("slug1", threadToChange);

            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(threadToChange, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("when given id or slug then return thread or 404")
    public void testInfo() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(id))
                    .thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(thread);

            ResponseEntity responseEntity = threadController.info(slug);

            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(thread, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("when given id or slug then return created vote")
    public void testCreateVote() {
        Vote vote = new Vote(username, 1);
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                threadDAOMock.when(() -> ThreadDAO.getThreadById(id))
                        .thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                        .thenReturn(thread);
                mockedUserDAO.when(() -> UserDAO.Info(username))
                        .thenReturn(user);

                ResponseEntity responseEntity = threadController.createVote(slug, vote);

                Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                Assertions.assertEquals(thread, responseEntity.getBody());
            }
        }
    }
}
